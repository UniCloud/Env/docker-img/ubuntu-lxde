FROM ubuntu

ENV DEBIAN_FRONTEND noninteractive

# Timezone
#https://rtfm.co.ua/en/docker-configure-tzdata-and-timezone-during-build/
ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#
RUN apt update
RUN apt install -y --no-install-recommends lxde-core
RUN apt install -y openssh-server
# <Fix>
RUN mkdir /run/sshd


# ssh: https://docs.docker.com/engine/examples/running_ssh_service/
RUN sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd
ENV NOTVISIBLE="in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile


# user: https://stackoverflow.com/questions/27701930/how-to-add-users-to-docker-container
RUN useradd -ms /bin/bash me
RUN echo 'me:u' | chpasswd
RUN echo 'root:u' | chpasswd

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
